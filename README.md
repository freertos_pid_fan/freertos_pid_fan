# FreeRTOS_PID_FAN

DEMO of using PID regulator to control FAN rotation speed.

Hardware:  
MCU: STM32F401CD (black pill board)  
Debug: ST-Link v2  
MOSFET switch module IRF520  
FAN: one old CPU cooler, rotation speed interval: 800-4800 rpm  
Speed sensor: IR encoder, 18 steps per rotation  

Software:  
STM32CubeMX - configuring  
STM32CubeIDE - development  
STM32CubeMonitor - monitoring (for setup PID's coefficients)  

FreeRTOS  
20kHZ PWM (to avoid accoustic noise), duty cycle 0-1400  
RB0 - input from encoder  
RA2 - PWM output  

Example graph in STM32CubeMonitor (after changing speed from 2000 rpm to 3000 rpm):
![image.png](./image.png)

Hardware:
![IMG_20220820_200108.jpg](./IMG_20220820_200108.jpg)
![IMG_20220820_200211.jpg](./IMG_20220820_200211.jpg)

